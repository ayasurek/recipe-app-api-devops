# setup S3 bucket to store lock state of terraform
terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-ay"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

# variables to be used in other terraform files, locals dont change value
# locals are usually derived from variables, other locals, resources, etc
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}

